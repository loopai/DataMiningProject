import numpy as np
import pandas as pd
from path import Path
import matplotlib.pyplot as plt
import seaborn as sns


def bar_plot(series, save_path=None, title=None):
    fig, ax = plt.subplots(figsize=(12, 6))
    sns.barplot(x=series.value_counts().index.tolist(),
                y=series.value_counts(),
                ax=ax)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(12)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(12)
    ax.yaxis.label.set_text('counts')
    ax.yaxis.label.set_fontsize(14)
    if title is not None:
        ax.title.set_text(title)
    else:
        ax.title.set_text(series.name)
    ax.title.set_fontsize(16)
    ax.title.set_fontweight('bold')
    if save_path is not None:
        plt.savefig(str(save_path), format='png')


def hist_plot(series, save_path=None, title=None):
    fig, ax = plt.subplots(figsize=(12, 6))
    sns.distplot(a=pd.Series(series.dropna(axis=0)), ax=ax)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(12)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(12)
    if title is not None:
        ax.title.set_text(title)
    else:
        ax.title.set_text(series.name)
    ax.title.set_fontsize(16)
    ax.title.set_fontweight('bold')
    if save_path is not None:
        plt.savefig(str(save_path), format='png')


def make_code_dict(series):
    unique_values = series.unique().tolist()
    unique_values.sort()
    code_dict = {"{}".format(key): num for num, key in enumerate(unique_values)}
    return code_dict


def encode(series_to_encode, code_dict):
    series_encoded = pd.Series([0] * series_to_encode.shape[0])
    idx = 0
    for key in series_to_encode:
        series_encoded[idx] = code_dict[key]
        idx += 1
    series_encoded.index = series_to_encode.index
    series_encoded.name = series_to_encode.name
    return series_encoded


def decode(series_to_decode, code_dict):
    series_decoded = pd.Series([' '] * series_to_decode.shape[0])
    idx = 0
    inv_code_dict = {v: k for k, v in code_dict.items()}
    for value in series_to_decode:
        series_decoded[idx] = inv_code_dict[value]
        idx += 1
    if type(series_to_decode) == pd.Series:
        series_decoded.index = series_to_decode.index
        series_decoded.name = series_to_decode.name
    else:
        print("warnings: the input array is not a pd.Series object.")
    return series_decoded


def highlight_esame_matematica_outliers(s, threshold=3):
    is_out = s < threshold
    return ['background-color: yellow' if v else '' for v in is_out]


def highlight_crediti_totali_outliers(s):
    '''
    This function is awful but i couldn't do better :C
    But given its utility i believe it is justified :D
    '''
    is_in = (70 < s) == (s < 120)
    return ['background-color: yellow' if v else '' for v in is_in]
