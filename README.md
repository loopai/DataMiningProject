# Data Mining Project

## Author: *Lorenzo Palloni*

## Report
- More ugly, but faster way [here](<./notebooks/main_notebook.ipynb>).

- More fancy, but slower way:

    * Download the html format of the report from [here](<./docs/main_notebook.html>).  
    * In Google Chrome a new tab will open with the source code in html.  
    * Just copy all the content (CTRL+A, then CTRL+C) and paste (CTRL+V) in a new file (the name doesn't matter).  
    * This file can finally be opened with any browser.

## Requirements
```
$pip install -r requirements.txt
```

## Project Structure
The project structure was inspired by [init_project](<https://gitlab.com/deeplego/init_project>) repository.